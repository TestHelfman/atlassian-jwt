package com.atlassian.jwt.core.keys;

import com.atlassian.jwt.exception.JwtCannotRetrieveKeyException;

import javax.annotation.Nonnull;
import java.security.interfaces.RSAPrivateKey;

public interface PrivateKeyRetriever
{
    enum keyLocationType
    {
        FILE, CLASSPATH_RESOURCE
    }

    @Nonnull
    RSAPrivateKey getPrivateKey() throws JwtCannotRetrieveKeyException;
}